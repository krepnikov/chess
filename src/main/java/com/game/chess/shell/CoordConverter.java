package com.game.chess.shell;

import com.game.chess.app.Coord;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * String to Coord converter.
 * Highly used for parsing commands from shell.
 * @see Converter
 */
@Component
public class CoordConverter implements Converter<String, Coord> {

    @Override
    public Coord convert(String value) {
        return Coord.create(value);
    }
}