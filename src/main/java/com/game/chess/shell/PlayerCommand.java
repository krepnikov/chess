package com.game.chess.shell;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.controller.PlayerController;
import com.game.chess.app.event.PlayerChangeEvent;
import com.game.chess.app.exception.GameFlowException;
import com.game.chess.app.exception.InvalidMoveException;
import com.game.chess.app.piece.Piece;
import org.springframework.context.event.EventListener;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;
import org.springframework.shell.standard.ShellOption;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static com.game.chess.shell.ConsoleColors.*;

/**
 * Player commands: all actions available for player in game.
 * This component listens for PlayerChangeEvent.
 */
@ShellComponent
public class PlayerCommand {

    public static final String WHITE_STYLE = WHITE_BOLD_BRIGHT + BLACK_BACKGROUND_BRIGHT;
    public static final String BLACK_STYLE = BLACK_BOLD_BRIGHT + WHITE_BACKGROUND_BRIGHT;
    public static final String AXES_STYLE = PURPLE;

    protected PlayerController player;

    @ShellMethod("I'm looking for move variants.")
    public void lk(@ShellOption Coord src) {
        try {
            Set<Coord> lookup = player.lookup(src);
            System.out.println(lookup.isEmpty() ? "No variants" : "Variants:");
            lookup.stream()
                .map(coord -> "\t- " + coord)
                .forEach(System.out::println);
        } catch (GameFlowException | InvalidMoveException e) {
            System.err.println(e.getMessage());
        }
    }

    @ShellMethod("Make move.")
    public void mv(@ShellOption Coord src, @ShellOption Coord trg) {
        try {
            player.move(src, trg);
        } catch (GameFlowException | InvalidMoveException e) {
            System.err.println(e.getMessage());
        }
    }
    @ShellMethod("Make castling.")
    public void castling(@ShellOption(value = "short", defaultValue = "true") boolean shrt) {
        try {
            player.castling(shrt);
        } catch (GameFlowException | InvalidMoveException e) {
            System.err.println(e.getMessage());
        }
    }


    @ShellMethod("Print board.")
    public void view(@ShellOption(value = {"rotate", "r"}, defaultValue = "false") boolean rotate) {
        try {
            List<List<Board.Cell>> matrix = new ArrayList<>(player.getBoardView());
            boolean reverse = rotate && player.getColor() == PieceColor.BLACK;
            if (!reverse) {
                Collections.reverse(matrix);
            }

            printAxesX();
            printDelimiter();

            AtomicInteger rowId = new AtomicInteger(reverse ? 1 : 8);
            Supplier<Integer> counter = reverse ? rowId::getAndIncrement : rowId::getAndDecrement;

            matrix.forEach(row -> {
                System.out.print("" + AXES_STYLE + counter.get() + RESET + "|");
                row.forEach(cell -> {
                    System.out.print('\t');
                    System.out.print( cell.isEmpty() ? "o" : getPieceCode(cell.getPiece()) );
                });
                System.out.println();
            });

            printDelimiter();
            printAxesX();
        } catch (GameFlowException e) {
            System.err.println(e.getMessage());
        }
    }

    private static String getPieceCode(Piece piece) {
        return (piece.getColor() == PieceColor.WHITE ? WHITE_STYLE : BLACK_STYLE) +
                piece.getCode() + RESET;
    }

    @ShellMethod("I give up.")
    public void giveUp() {
        try {
            player.giveUp();
        } catch (GameFlowException e) {
            System.err.println(e.getMessage());
        }
    }

    @EventListener
    public void onPlayerChange(PlayerChangeEvent event) {
        this.player = event.getSource();
    }

    @ShellMethodAvailability
    public Availability availability() {
        return player != null ? Availability.available() : Availability.unavailable("Game is not started");
    }

    private static void printAxesX() {
        System.out.print(AXES_STYLE);
        IntStream.rangeClosed('a', 'h')
                .forEachOrdered(label -> System.out.print("\t" + (char)label));
        System.out.println(RESET);
    }
    private static void printDelimiter() {
        String[] parts = new String[8];
        Arrays.fill(parts,"----");
        System.out.println("  " + String.join("", parts));
    }
}
