package com.game.chess.shell;

import com.game.chess.app.controller.GameController;
import com.game.chess.app.exception.GameFlowException;
import com.game.chess.app.log.GameLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellMethodAvailability;

/**
 * Game shell commands for setting and game management.
 */
@ShellComponent
public class GameCommand {

    @Autowired
    protected GameController game;
    @Autowired
    protected GameLog logger;

    @ShellMethod("Start game")
    public void start() {
        try {
            game.start();
        } catch (GameFlowException e) {
            System.err.println(e.getMessage());
        }
    }

    @ShellMethod("Print log.")
    public void log() {
        logger.getLog().stream()
                .map(msg -> String.format("%s: %s", msg.getType(), msg.getMessage()))
                .forEach(System.out::println);
    }

    @ShellMethodAvailability({"start"})
    public Availability startAvailability() {
        return game.isInProgress() ? Availability.unavailable("Game is in progress") : Availability.available();
    }
}
