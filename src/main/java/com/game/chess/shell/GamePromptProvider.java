package com.game.chess.shell;

import com.game.chess.app.controller.GameController;
import com.game.chess.app.controller.PlayerController;
import com.game.chess.app.event.PlayerChangeEvent;
import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

import static org.jline.utils.AttributedStyle.*;

/**
 * Change game prompt and it's style for different game states.
 */
@Component
public class GamePromptProvider implements PromptProvider {

    @Autowired
    private GameController game;
    private PlayerController player;

    @Override
    public AttributedString getPrompt() {
        if (game.isInProgress()) {
            if (player != null) {
                return new AttributedString(getPlayerName() + ":>", player.isWhite() ? DEFAULT : INVERSE);
            } else {
                return new AttributedString("started:>", DEFAULT.foreground(BLUE));
            }
        }
        else {
            return new AttributedString("chess:>", DEFAULT.foreground(GREEN));
        }
    }

    @EventListener
    public void onPlayerChange(PlayerChangeEvent event) {
        this.player = event.getSource();
    }

    private String getPlayerName() {
        return player.getColor().name().toLowerCase();
    }
}
