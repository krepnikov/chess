package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.util.SpaceUtil;

import java.util.HashSet;
import java.util.Set;

public class King extends Piece {

    public King(PieceColor color) {
        super(color);
    }

    @Override
    public String getName() {
        return "King";
    }

    @Override
    public String getCode() {
        return "K";
    }

    @Override
    public Set<Coord> getAttackArea(Board board, Coord coord) {
        HashSet<Coord> area = new HashSet<>();
        SpaceUtil.all()
            .map(v -> board.get(coord, v.x, v.y))
            .filter(Board.Cell::isValid)
            .map(Board.Cell::getCoord)
            .forEach(area::add);
        return area;
    }
}
