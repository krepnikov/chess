package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.util.MoveAreaUtil;
import com.game.chess.app.util.SpaceUtil;

import java.util.Set;

public class Queen extends Piece {

    public Queen(PieceColor color) {
        super(color);
    }

    @Override
    public String getName() {
        return "Queen";
    }

    @Override
    public String getCode() {
        return "Q";
    }

    @Override
    public Set<Coord> getAttackArea(Board board, Coord coord) {
        return MoveAreaUtil.find(board, coord, this, SpaceUtil.all());
    }
}
