package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.util.SpaceUtil;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Knight extends Piece {

    public Knight(PieceColor color) {
        super(color);
    }

    @Override
    public String getName() {
        return "Knight";
    }

    @Override
    public String getCode() {
        return "N";
    }

    @Override
    public Set<Coord> getAttackArea(Board board, Coord coord) {
        return Stream.of(
                new SpaceUtil.Vec(1, 2),
                new SpaceUtil.Vec(2, 1),
                new SpaceUtil.Vec(-1, 2),
                new SpaceUtil.Vec(-2, 1),
                new SpaceUtil.Vec(1, -2),
                new SpaceUtil.Vec(2, -1),
                new SpaceUtil.Vec(-1, -2),
                new SpaceUtil.Vec(-2, -1)
        )
                .map(v -> board.get(coord, v.x, v.y))
                .filter(Board.Cell::isValid)
                .filter(cell -> cell.isEmpty() || cell.getPiece().isEnemy(color))
                .map(Board.Cell::getCoord)
                .collect(Collectors.toSet());
    }
}
