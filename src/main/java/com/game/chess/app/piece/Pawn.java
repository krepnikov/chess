package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Pawn extends Piece {

    public Pawn(PieceColor color) {
        super(color);
    }

    @Override
    public String getName() {
        return "Pawn";
    }

    @Override
    public String getCode() {
        return "p";
    }

    @Override
    public Set<Coord> getAttackArea(Board board, Coord coord) {
        final Board boardView = isWhite() ? board : board.inverse();

        HashSet<Coord> area = new HashSet<>();
        // check simple move - 1 cell forward.
        Board.Cell face = checkJump(boardView, coord, area);
        // if it is first move of pawn, check long move - 2 cell forward.
        checkLongJump(boardView, coord, area, face);
        // if there is enemy, check ability to attack.
        checkAttack(boardView, coord, area);

        return area;
    }


    private Board.Cell checkJump(Board boardView, Coord coord, HashSet<Coord> area) {
        Board.Cell face = boardView.get(coord, 0, 1);
        if (face.isValid() && face.isEmpty()) {
            area.add(face.getCoord());
        }
        return face;
    }
    private void checkLongJump(Board boardView, Coord coord, HashSet<Coord> area, Board.Cell face) {
        if (isInitPos(coord) && face.isValid() && face.isEmpty()) {
            Board.Cell longJump = boardView.get(coord, 0, 2);
            if (longJump.isEmpty()) {
                area.add(longJump.getCoord());
            }
        }
    }
    private void checkAttack(Board boardView, Coord coord, HashSet<Coord> area) {
        area.addAll(
                Stream.of(1, -1)
                        .map(dx -> boardView.get(coord, dx, 1))
                        .filter(Board.Cell::isValid)
                        .filter(cell -> !cell.isEmpty())
                        .filter(cell -> cell.getPiece().isEnemy(color))
                        .map(Board.Cell::getCoord)
                        .collect(Collectors.toSet())
        );
    }

    public boolean isInitPos(Coord coord) {
        return 1 == (isWhite() ? coord.getY() : 7 - coord.getY());
    }

    public boolean isWhite() {
        return color == PieceColor.WHITE;
    }
}
