package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;

import java.util.Set;

/**
 * Abstract Piece.
 * Each piece should extend and use current contract.
 * All pieces can be found in current package.
 */
public abstract class Piece {
    protected final PieceColor color;

    /**
     * @param color piece color is required, because for some pieces move direction
     *              and restriction depends on color.
     */
    public Piece(PieceColor color) {
        this.color = color;
    }

    /**
     * Piece readable name.
     */
    public abstract String getName();
    /**
     * Piece readable code for game logs.
     */
    public abstract String getCode();

    public PieceColor getColor() {
        return color;
    }
    /**
     * Is current color belongs to enemy?
     */
    public boolean isEnemy(PieceColor color) {
        return !this.color.equals(color);
    }

    /**
     * Calculates feasible area for move.
     * Note: this method can be used as strategy.
     * Piece know nothing about it's location or board state.
     * You can use it for calculations and predictions in advance.
     *
     * @param board state - context.
     * @param coord current piece location.
     * @return set of feasible cell coordinates.
     */
    public abstract Set<Coord> getAttackArea(Board board, Coord coord);

}
