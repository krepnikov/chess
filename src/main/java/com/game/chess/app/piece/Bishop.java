package com.game.chess.app.piece;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.util.MoveAreaUtil;
import com.game.chess.app.util.SpaceUtil;

import java.util.Set;

public class Bishop extends Piece {

    public Bishop(PieceColor color) {
        super(color);
    }

    @Override
    public String getName() {
        return "Bishop";
    }

    @Override
    public String getCode() {
        return "B";
    }

    @Override
    public Set<Coord> getAttackArea(final Board board, Coord coord) {
        return MoveAreaUtil.find(board, coord, this, SpaceUtil.diag());
    }
}
