package com.game.chess.app.util;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.piece.Piece;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Util class for calculation move/attack area.
 * Most of pieces can delegate operation to it.
 * Main idea: we travers cells in specific directions starting from current piece,
 *            until end of board or another piece was met. If piece is enemy,
 *            it is feasible for attack.
 */
public class MoveAreaUtil {
    /**
     * @param board current board.
     * @param coord current piece location.
     * @param piece current piece.
     * @param vectors directions (unit vector) for traversing.
     * @return
     */
    public static Set<Coord> find(Board board, Coord coord, Piece piece, Stream<SpaceUtil.Vec> vectors) {
        HashSet<Coord> area = new HashSet<>();
        vectors.forEach(v -> {
            int i = 0;
            Board.Cell cell;
            do {
                i++;
                cell = board.get(coord, v.x * i, v.y * i);
                if (cell.isValid() && (cell.isEmpty() || cell.getPiece().isEnemy(piece.getColor()))) {
                    area.add(cell.getCoord());
                }
            } while (cell.isValid() && cell.isEmpty());
        });
        return area;
    }
}
