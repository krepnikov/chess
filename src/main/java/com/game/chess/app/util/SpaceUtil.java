package com.game.chess.app.util;

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Space util provides methods for retrieving unit vector sets.
 */
public class SpaceUtil {

    /**
     * All directions.
     * @see com.game.chess.app.piece.Queen
     * @see com.game.chess.app.piece.King
     */
    public static Stream<Vec> all() {
        return IntStream.rangeClosed(-1, 1).mapToObj(dx ->
                IntStream.rangeClosed(-1, 1).mapToObj(dy -> new Vec(dx, dy))
        )
        .flatMap(Function.identity())
        .filter(v -> v.x != 0 || v.y != 0);
    }

    /**
     * Diagonals.
     * @see com.game.chess.app.piece.Bishop
     */
    public static Stream<Vec> diag() {
        return all().filter(v -> v.x != 0 && v.y != 0);
    }

    /**
     * Vertical and horizontal axes.
     * @see com.game.chess.app.piece.Rook
     */
    public static Stream<Vec> direct() {
        return all().filter(v -> v.x == 0 || v.y == 0);
    }

    public static class Vec {
        public final int x;
        public final int y;

        public Vec(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}


