package com.game.chess.app.controller;

import com.game.chess.app.Board;
import com.game.chess.app.PieceColor;
import com.game.chess.app.event.BoardCreateEvent;
import com.game.chess.app.event.FinishGameEvent;
import com.game.chess.app.event.PlayerChangeEvent;
import com.game.chess.app.exception.GameFlowException;
import com.game.chess.app.loader.GameInitializer;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 * Main Game controller controls game state, game flow and board.
 *
 * @see PlayerController
 * @see Board
 */
@Component
public class GameController {
    enum State { READY, PROGRESS, DONE }

    private State state = State.READY;
    private PlayerController moveOwner;
    private Iterator<PlayerController> players;

    private Board board;

    @Autowired
    private GameInitializer initializer;
    @Autowired
    private ApplicationEventPublisher publisher;

    // Main game flow actions.

    /**
     * Start new game.
     * Is allowed only if game is not in progress.
     * This action will create new board, player controllers.
     */
    public void start() throws GameFlowException {
        if (this.state == State.PROGRESS) {
            throw new GameFlowException("Game in progress");
        }
        this.state = State.PROGRESS;

        this.board = new Board();
        initializer.init(this.board);

        publisher.publishEvent(new BoardCreateEvent(this.board));

        players = IterableUtils.loopingIterable(
                Arrays.stream(PieceColor.values())
                    .map(color -> new PlayerController(this, color, publisher))
                    .collect(Collectors.toList())
        ).iterator();

        setMoveOwner(players.next());
    }

    /**
     * This method finishes the game.
     * @param winner player wins the game.
     */
    public void finish(PlayerController winner) throws GameFlowException {
        if (this.state != State.PROGRESS) {
            throw new GameFlowException("Game is not in progress");
        }
        this.state = State.DONE;
        this.moveOwner = winner;
        publisher.publishEvent(new FinishGameEvent(this.moveOwner));
    }

    /**
     * Player can give up, if it is his turn to make move.
     * Another player becomes winner.
     * @param player who want to give up.
     */
    public void giveUp(PlayerController player) throws GameFlowException {
        if (!this.moveOwner.equals(player)) {
            throw new GameFlowException("Wait for your turn!");
        }
        this.state = State.DONE;
        this.moveOwner = players.next();
        publisher.publishEvent(new FinishGameEvent(this.moveOwner, true));
    }

    // Access control to Game Board.

    /**
     * Player can request access to Board in his turn.
     * @param player current active player
     * @return
     */
    public Board getBoard(PlayerController player) throws GameFlowException {
        if (!this.moveOwner.equals(player)) {
            throw new GameFlowException("Wait for your turn!");
        }
        return board;
    }

    /**
     * When move is completed, player should release board for next move.
     * @param player current active player.
     */
    public void releaseBoard(PlayerController player) throws GameFlowException {
        if (!this.moveOwner.equals(player)) {
            throw new GameFlowException("It is not your turn!");
        }
        setMoveOwner(players.next());
    }

    // Other methods.

    /**
     * Change active player and publish event about it.
     * @param player next active player.
     */
    private void setMoveOwner(PlayerController player) {
        this.moveOwner = player;
        publisher.publishEvent(new PlayerChangeEvent(this.moveOwner));
    }

    /**
     * @return is game in progress now?
     */
    public boolean isInProgress() {
        return this.state == State.PROGRESS;
    }
}
