package com.game.chess.app.controller;

import com.game.chess.app.Board;
import com.game.chess.app.Coord;
import com.game.chess.app.PieceColor;
import com.game.chess.app.event.AfterMoveEvent;
import com.game.chess.app.event.BeforeMoveEvent;
import com.game.chess.app.exception.GameFlowException;
import com.game.chess.app.exception.InvalidMoveException;
import com.game.chess.app.piece.Piece;
import org.springframework.context.ApplicationEventPublisher;

import java.util.List;
import java.util.Set;

/**
 * Basic Player controller provides implementation for all player actions.
 * Current actions are:
 * - lookup
 * - move
 * - get board matrix view
 *
 * Each player should have his own player controller.
 *
 */
public class PlayerController {
    private final GameController game;
    private final PieceColor color;
    private final ApplicationEventPublisher publisher;

    public PlayerController(GameController game, PieceColor color, ApplicationEventPublisher publisher) {
        this.game = game;
        this.color = color;
        this.publisher = publisher;
    }

    /**
     * Provides list of available moves from current cell.
     * If cell is empty (without piece), it throws exception InvalidMoveException.
     * Note: you cane lookup opponent's possible moves too.
     *
     * @param from selected cell.
     * @return set of feasible cell locations.
     * @throws InvalidMoveException if from cell is empty.
     */
    public Set<Coord> lookup(Coord from) throws InvalidMoveException, GameFlowException {
        Board board = this.game.getBoard(this);
        Board.Cell cell = board.get(from);
        if (cell.isEmpty()) {
            throw new InvalidMoveException("Empty cell");
        } else {
            return cell.getPiece().getAttackArea(board, from);
        }
    }

    /**
     * Move piece to new location.
     * Current action includes simple move and attack move, both.
     * In case of attack new location is not empty - there is opponent's piece.
     * If cell is empty (without piece), it throws exception InvalidMoveException.
     *
     * @param from move piece from this cell.
     * @param to move piece to this cell.
     * @throws InvalidMoveException if from cell is empty.
     * @throws InvalidMoveException if from cell contains opponent's piece.
     * @throws InvalidMoveException if to cell is not feasible.
     */
    public void move(Coord from, Coord to) throws InvalidMoveException, GameFlowException {
        Board board = this.game.getBoard(this);
        Board.Cell fromCell = board.get(from);
        Board.Cell toCell = board.get(to);
        Piece fromPiece = fromCell.getPiece();
        Piece toPiece = toCell.getPiece();

        if (fromCell.isEmpty()) {
            throw new InvalidMoveException("Empty cell");
        }
        if (fromPiece.isEnemy(color)) {
            throw new InvalidMoveException("Not your piece");
        }

        Set<Coord> area = fromCell.getPiece().getAttackArea(board, from);
        if (!area.contains(to)) {
            throw new InvalidMoveException(String.format("You cannot move to %s", to));
        }

        publisher.publishEvent(new BeforeMoveEvent(board, fromCell, toCell));
        toCell.setPiece(fromCell.getPiece());
        publisher.publishEvent(new AfterMoveEvent(board, fromCell, toCell, fromPiece, toPiece));
        this.game.releaseBoard(this);
    }

    public void castling(boolean shrt) throws InvalidMoveException, GameFlowException {
        //TODO: implement kingside and queenside castling.
    }

    /**
     * Just give up.
     */
    public void giveUp() throws GameFlowException {
        this.game.giveUp(this);
    }

    /**
     * @return get board matrix view for visualization.
     */
    public List<List<Board.Cell>> getBoardView() throws GameFlowException {
        return this.game.getBoard(this).getMatrixView();
    }

    /**
     * @return current player color.
     */
    public PieceColor getColor() {
        return color;
    }
    public boolean isWhite() {
        return getColor() == PieceColor.WHITE;
    }
    public boolean isBlack() {
        return getColor() == PieceColor.BLACK;
    }
}
