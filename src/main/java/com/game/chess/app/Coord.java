package com.game.chess.app;

import java.util.Objects;
import java.util.WeakHashMap;
import java.util.function.Predicate;
import java.util.regex.Pattern;

/**
 * Chess coordinate class defines formatting and parsing of coordinates.
 * Regular chess board has size 8x8 with cell naming [a-h][1-8].
 * This class defines coordinate system x / y,
 * where x may be provided in both formats: letters and digits.
 * Note:
 * - board size constraints are defined in Board class.
 * - Coord uses internal instance caching, which optimize object reuse, instead of recreation.
 */
public class Coord {
    private static final WeakHashMap<String, Coord> CACHE = new WeakHashMap<>();

    public static final Predicate<String> VALIDATOR = Pattern
            .compile("[a-h][1-8]", Pattern.CASE_INSENSITIVE)
            .asPredicate();
    public static final char A = 'a';

    /**
     * Create coord from x / y digits.
     * @param x
     * @param y
     * @return
     */
    public static Coord create(int x, int y) {
        String hash = key(x, y);
        Coord val = CACHE.get(hash);
        if (val == null) {
            String value = (char)(A + x) + "" + (y + 1);
            val = new Coord(value, x, y);
            updateCache(val);
        }
        return val;
    }

    /**
     * Create coord from x / y chess traditional format.
     * @param value [a-h][1-8] case insensitive.
     * @return
     */
    public static Coord create(String value) {
        value = normalize(value);
        Coord val = CACHE.get(value);
        if (val == null) {
            if (!VALIDATOR.test(value)) {
                return new Coord(value);
            }
            int x = value.charAt(0) - A;
            int y = Integer.parseInt("" + value.charAt(1)) - 1;
            val = new Coord(value, x, y);
            updateCache(val);
        }
        return val;
    }

    private static String normalize(String value) {
        return value.toLowerCase();
    }
    private static String key(int x, int y) {
        return x + ":" + y;
    }
    private static void updateCache(Coord val) {
        CACHE.put(val.value, val);
        CACHE.put(key(val.x, val.y), val);
    }

    private String value;
    private int x;
    private int y;

    private Coord(String value) {
        this(value, -1, -1);
    }
    private Coord(String value, int x, int y) {
        assert value != null;
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public String getValue() {
        return value;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coord coord = (Coord) o;
        return Objects.equals(value, coord.value);
    }

    @Override
    public int hashCode() {
        return this.getValue().hashCode();
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
