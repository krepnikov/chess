package com.game.chess.app.exception;

/**
 * Exceptional case in player actions:
 * - you cannot make such move.
 * - it is not your chess piece.
 * - no piece in cell - nothing to move.
 * etc.
 */
public class InvalidMoveException extends Exception {
    public InvalidMoveException(String message) {
        super(message);
    }
}
