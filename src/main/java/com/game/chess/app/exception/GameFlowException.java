package com.game.chess.app.exception;

/**
 * Exceptional case in game flow.
 */
public class GameFlowException extends Exception {
    public GameFlowException(String message) {
        super(message);
    }
}
