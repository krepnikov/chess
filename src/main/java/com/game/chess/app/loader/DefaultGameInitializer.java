package com.game.chess.app.loader;

import com.game.chess.app.Board;
import com.game.chess.app.PieceColor;
import com.game.chess.app.piece.*;

import java.util.stream.IntStream;

/**
 * Default board loader provides piece start location.
 */
public class DefaultGameInitializer implements GameInitializer {
    @Override
    public void init(Board board) {
        for (PieceColor color : PieceColor.values()) {
            Board view = color == PieceColor.WHITE ? board : board.inverse();
            view.get(0, 0).setPiece(new Rook(color));
            view.get(7, 0).setPiece(new Rook(color));
            view.get(1, 0).setPiece(new Bishop(color));
            view.get(6, 0).setPiece(new Bishop(color));
            view.get(2, 0).setPiece(new Knight(color));
            view.get(5, 0).setPiece(new Knight(color));
            view.get(3, 0).setPiece(new King(color));
            view.get(4, 0).setPiece(new Queen(color));
            IntStream.rangeClosed(0, 7)
                    .forEach(x -> view.get(x, 1).setPiece(new Pawn(color)));
        }
    }
}
