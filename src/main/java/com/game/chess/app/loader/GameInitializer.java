package com.game.chess.app.loader;

import com.game.chess.app.Board;

/**
 * Provides game board initial state loading.
 * Can be used to load specific board state from file or cloud.
 * Note: on load from file, piece creation should be implemented
 *       using factory method pattern.
 */
public interface GameInitializer {
    void init(Board board);
}
