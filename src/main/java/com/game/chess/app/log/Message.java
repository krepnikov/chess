package com.game.chess.app.log;

public interface Message {

    Type getType();
    String getMessage();

    enum Type {
        MOVE, INFO, STATUS
    }
}
