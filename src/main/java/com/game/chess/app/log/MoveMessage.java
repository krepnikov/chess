package com.game.chess.app.log;

import com.game.chess.app.PieceColor;
import com.game.chess.app.event.AfterMoveEvent;

import static java.util.Objects.isNull;

public class MoveMessage implements Message {
    private String message;

    public MoveMessage(AfterMoveEvent event) {
        this.message = String.format("%s> %s%s %s%s",
                event.getColor().printName(),
                event.getFromPiece().getCode(),
                event.getFromCell().getCoord().getValue(),
                isNull(event.getToPiece()) ? "" : event.getToPiece().getCode(),
                event.getToCell().getCoord().getValue()
        );
    }

    public MoveMessage(PieceColor color, String message) {
        this.message = String.format("%s> %s", color.name().toLowerCase(), message);
    }

    @Override
    public Type getType() {
        return Type.MOVE;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
