package com.game.chess.app.log;

import com.game.chess.app.event.AfterMoveEvent;

public class KillMessage implements Message {
    private String message;

    public KillMessage(AfterMoveEvent event) {
        assert event.getToPiece() != null;
        this.message = String.format("%s %s takes %s %s",
                event.getFromPiece().getColor().printName(),
                event.getFromPiece().getName(),
                event.getToPiece().getColor().printName(),
                event.getToPiece().getName()
        );
    }

    @Override
    public Type getType() {
        return Type.INFO;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
