package com.game.chess.app.log;

public class StatusMessage implements Message {
    private final String message;

    public StatusMessage(String message) {
        this.message = message;
    }

    @Override
    public Type getType() {
        return Type.STATUS;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
