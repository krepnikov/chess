package com.game.chess.app.log;

import com.game.chess.app.event.AfterMoveEvent;
import com.game.chess.app.event.BoardCreateEvent;
import com.game.chess.app.event.FinishGameEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.game.chess.app.PieceColor.another;
import static java.lang.String.format;
import static java.util.Collections.unmodifiableList;

@Component
public class GameLog {
    private List<Message> log = new ArrayList<>();

    @EventListener
    public void onMove(AfterMoveEvent event) {
        log.add(new MoveMessage(event));
        if (event.getToPiece() != null) {
            log.add(new KillMessage(event));
        }
    }

    @EventListener
    public void onBoard(BoardCreateEvent event) {
        log = new ArrayList<>();
        log.add(new StatusMessage("Game started!"));
    }

    @EventListener
    public void onFinish(FinishGameEvent event) {
        if (event.getWinner() == null) {
            log.add(new StatusMessage("Draw, not bad!"));
        } else {
            String giveUpMsg = event.isGiveUp() ?
                    format("%s gives up.", another(event.getWinner()).printName()) : "";
            String winMsg = format("Congrats, %s is winner!", event.getWinner().printName());
            log.add(new StatusMessage(giveUpMsg + winMsg));
        }
    }

    public List<Message> getLog() {
        return unmodifiableList(log);
    }
}
