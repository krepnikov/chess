package com.game.chess.app;

import com.game.chess.app.piece.Piece;
import org.springframework.context.ApplicationEventPublisher;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;

/**
 * Board class defines game space and provides game state for any moment.
 * By default chess board space looks like 8x8 matrix. But you can implement dozens variants
 * of classic chess defining custom variants of game space (without changing other rules),
 * for example 3d chess board, octagon-space or open-space board.
 */
public class Board {
    private static final int SIZE = 8;

    private final Cell INVALID = new Cell(Coord.create("err"));
    private final Map<Coord, Cell> cells;
    private final List<List<Cell>> matrixView;

    private ApplicationEventPublisher publisher;

    public Board() {
        LinkedHashMap<Coord, Cell> cells = new LinkedHashMap<>();
        List<List<Cell>> matrixView = new ArrayList<>(SIZE);

        // iterate and create rows
        IntStream.rangeClosed(0, SIZE - 1).forEachOrdered(y -> {
            // temp row
            ArrayList<Cell> row = new ArrayList<>(SIZE);

            // create cells for row
            IntStream.rangeClosed(0, SIZE - 1).forEachOrdered(x -> {
                Cell cell = new Cell(Coord.create(x, y));
                row.add(cell);
                cells.put(cell.getCoord(), cell);
            });

            // row is unmodifiable.
            matrixView.add(unmodifiableList(row));
        });

        // board structure and view should be unmodifiable.
        this.cells = unmodifiableMap(cells);
        this.matrixView = unmodifiableList(matrixView);
    }

    /**
     * Get inverse by y board (for opponent view).
     * @return
     */
    public Board inverse() {
        return new InverseBoard();
    }
    /**
     * Get cell by absolute coordinates.
     * Note: even if coordinates are invalid, you will recieve object.
     * @param coord
     * @return
     */
    public Cell get(Coord coord) {
        return this.getCells().getOrDefault(coord, INVALID);
    }
    /**
     * Get cell by separate digit coordinates.
     * If board is inverted, (0,0) is equal to regular (0,7).
     * @param x
     * @param y
     * @return
     */
    public Cell get(int x, int y) {
        return this.get(Coord.create(x, y));
    }

    /**
     * Get cell by absolute coordinates and relative shift.
     * @param coord
     * @param dx
     * @param dy
     * @return
     */
    public Cell get(Coord coord, int dx, int dy) {
        return this.get(
                Coord.create(coord.getX() + dx,coord.getY() + dy)
        );
    }

    /**
     * Matrix view for visualization.
     * Note: will be nice to provide View as separate entity.
     * @return
     */
    public List<List<Cell>> getMatrixView() {
        return matrixView;
    }

    public class Cell {
        private final Coord coord;
        private Piece piece;

        public Cell(Coord coord) {
            this.coord = coord;
        }

        public Coord getCoord() {
            return coord;
        }

        public Piece getPiece() {
            return piece;
        }

        public void setPiece(Piece piece) {
            if (Objects.isNull(piece)) {
                this.piece = null;
                return;
            }

            // before setting piece to cell, lets remove it from another.
            Board.this.getCells().values().stream()
                    .filter(cell -> !cell.isEmpty())
                    .filter(cell -> cell.getPiece().equals(piece))
                    .forEach(cell -> cell.setPiece(null));

            this.piece = piece;
        }

        public boolean isEmpty() {
            return this.piece == null;
        }

        public boolean isValid() {
            return this.coord != null &&
                    this.coord.getX() >= 0 && this.coord.getX() < SIZE &&
                    this.coord.getY() >= 0 && this.coord.getY() < SIZE;
        }
    }

    /**
     * Board inverse view for 2nd player.
     */
    public class InverseBoard extends Board {
        private InverseBoard() {}

        @Override
        public Map<Coord, Cell> getCells() {
            return Board.this.getCells();
        }

        @Override
        public List<List<Cell>> getMatrixView() {
            return Board.this.getMatrixView();
        }

        @Override
        public Cell get(int x, int y) {
            return super.get(x,SIZE - 1 - y);
        }
        
        @Override
        public Cell get(Coord coord, int dx, int dy) {
            return super.get(coord, dx, -dy);
        }

        @Override
        public Board inverse() {
            return Board.this;
        }
    }

    protected Map<Coord, Cell> getCells() {
        return cells;
    }

    public void setPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }
}
