package com.game.chess.app.event;

import com.game.chess.app.Board;
import com.game.chess.app.PieceColor;
import com.game.chess.app.piece.Piece;
import org.springframework.context.ApplicationEvent;

public class AfterMoveEvent extends ApplicationEvent {
    private final Board.Cell fromCell;
    private final Board.Cell toCell;
    private final Piece fromPiece;
    private final Piece toPiece;

    public AfterMoveEvent(Board source,
                          Board.Cell fromCell, Board.Cell toCell,
                          Piece fromPiece, Piece toPiece)
    {
        super(source);
        this.fromCell = fromCell;
        this.toCell = toCell;
        this.fromPiece = fromPiece;
        this.toPiece = toPiece;
    }

    @Override
    public Board getSource() {
        return (Board) super.getSource();
    }

    public Board.Cell getFromCell() {
        return fromCell;
    }

    public Board.Cell getToCell() {
        return toCell;
    }

    public Piece getFromPiece() {
        return fromPiece;
    }

    public Piece getToPiece() {
        return toPiece;
    }

    public PieceColor getColor() {
        return this.fromPiece.getColor();
    }
}
