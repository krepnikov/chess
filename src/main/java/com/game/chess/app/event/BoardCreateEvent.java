package com.game.chess.app.event;

import com.game.chess.app.Board;
import org.springframework.context.ApplicationEvent;

public class BoardCreateEvent extends ApplicationEvent {
    public BoardCreateEvent(Board source) {
        super(source);
    }

    @Override
    public Board getSource() {
        return (Board) super.getSource();
    }
}
