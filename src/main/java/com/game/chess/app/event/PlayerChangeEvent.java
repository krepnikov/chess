package com.game.chess.app.event;

import com.game.chess.app.controller.PlayerController;
import org.springframework.context.ApplicationEvent;

public class PlayerChangeEvent extends ApplicationEvent {
    public PlayerChangeEvent(PlayerController source) {
        super(source);
    }

    @Override
    public PlayerController getSource() {
        return (PlayerController) super.getSource();
    }
}
