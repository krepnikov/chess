package com.game.chess.app.event;

import com.game.chess.app.Board;
import com.game.chess.app.PieceColor;
import org.springframework.context.ApplicationEvent;

public class BeforeMoveEvent extends ApplicationEvent {
    private final Board.Cell fromCell;
    private final Board.Cell toCell;

    public BeforeMoveEvent(Board source,
                           Board.Cell fromCell, Board.Cell toCell)
    {
        super(source);
        this.fromCell = fromCell;
        this.toCell = toCell;
    }

    @Override
    public Board getSource() {
        return (Board) super.getSource();
    }

    public Board.Cell getFromCell() {
        return fromCell;
    }

    public Board.Cell getToCell() {
        return toCell;
    }

    public PieceColor getColor() {
        return this.fromCell.getPiece().getColor();
    }

}
