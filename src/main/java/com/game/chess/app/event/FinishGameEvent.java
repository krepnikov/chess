package com.game.chess.app.event;

import com.game.chess.app.PieceColor;
import com.game.chess.app.controller.PlayerController;
import org.springframework.context.ApplicationEvent;

public class FinishGameEvent extends ApplicationEvent {
    private boolean giveUp = false;

    public FinishGameEvent(PlayerController source) {
        super(source);
    }

    public FinishGameEvent(PlayerController source, boolean giveUp) {
        super(source);
        this.giveUp = giveUp;
    }

    @Override
    public PlayerController getSource() {
        return (PlayerController) super.getSource();
    }

    public PieceColor getWinner() {
        return getSource() != null ? getSource().getColor() : null;
    }

    public boolean isGiveUp() {
        return giveUp;
    }
}
