package com.game.chess.app.service;

import com.game.chess.app.Board;
import com.game.chess.app.controller.GameController;
import com.game.chess.app.event.AfterMoveEvent;
import com.game.chess.app.event.BoardCreateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class CheckMateService {

    @Autowired
    private GameController game;
    @Autowired
    private ApplicationEventPublisher publisher;

    private Board board;

    @EventListener
    public void onMove(AfterMoveEvent event) {
        // determine Check and CheckMate
        // publish event about state
        // game.finish(...);
    }

    @EventListener
    public void onBoard(BoardCreateEvent event) {
        this.board = event.getSource();
    }
}
