package com.game.chess.app.service;

import com.game.chess.app.Board;
import com.game.chess.app.event.AfterMoveEvent;
import com.game.chess.app.event.BoardCreateEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class PawnToQueenService {

    @Autowired
    private ApplicationEventPublisher publisher;

    private Board board;

    @EventListener
    public void onMove(AfterMoveEvent event) {
        // determine Pawn in 8th rank: it should be replaced with Queen, if it was already taken.
        // publish event
    }

    @EventListener
    public void onBoard(BoardCreateEvent event) {
        this.board = event.getSource();
    }
}
