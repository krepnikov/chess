package com.game.chess.app;

import org.springframework.util.StringUtils;

/**
 * Player and piece colors.
 */
public enum PieceColor {
    WHITE, BLACK;

    public static PieceColor another(PieceColor color) {
        return color == WHITE ? BLACK : WHITE;
    }

    public String printName() {
        return StringUtils.capitalize(name().toLowerCase());
    }
}
