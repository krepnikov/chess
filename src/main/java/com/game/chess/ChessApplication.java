package com.game.chess;

import com.game.chess.app.loader.DefaultGameInitializer;
import com.game.chess.app.loader.GameInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ChessApplication {

    @Bean
    @ConditionalOnMissingBean
    public GameInitializer defaultGameInitializer() {
        return new DefaultGameInitializer();
    }

    public static void main(String[] args) {
        SpringApplication.run(ChessApplication.class, args);
    }
}
